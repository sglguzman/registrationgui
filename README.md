# registrationgui

### Sergio Guzman-Lara

# Overview

 * This is my (very simple) implementation of coding exercise number 5. 
 * I knwow that it was requested to use React, but I do not have any experience with React, so I decided to use Angular instead.
 * This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2, bootstrap 4.3.1 and npm 6.9.0
 * Simple validations are implemented for the User's fields


## To run

 * `git clone https://gitlab.com/sglguzman/registrationgui.git`
 * `cd registrationgui`
 * `npm install`
 * `ng serve --open`
 * Will run at `http://localhost:4200/` 
