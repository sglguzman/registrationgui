import { Component } from '@angular/core';
import { NotificationComponent } from './components/notification/notification.component';
import { UserformComponent } from './components/userform/userform.component';

import { User } from './models/User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showNotification:boolean = false;
  currentMessage:string = '';
  title:string = 'registrationgui (Coding exercise 5)';

  enabledLogin:boolean= true;
  enabledLogout:boolean=true;
  enabledList:boolean=true;
  enabledFind:boolean=true;
  enabledAdd:boolean=true;
  showUser:boolean=false;

  userFormUser:User ={
    firstName:'',
    lastName:'',
    npiNumber:'',
    phone:'',
    email:'',
    businessAddress:''
  };

  notify(m: string) {
    this.currentMessage = m;
    this.showNotification = true;
  }

  reset() {
    this.currentMessage='';
    this.showNotification=false;
  }

  notImplemented() {
    this.currentMessage = 'Feature not implemented yet...';
    this.showNotification = true;
  }

  handleMessage(s: string) {
    console.log(" Notification is [" + s + "]");
    this.showNotification = false;
  }

  addNewUser(user:User) {
    this.showUser=false;
    this.notify('Here we would call the back server to save:\n'+ JSON.stringify(this.userFormUser));
  }

  processAddUser() {
    console.log('About to add a user');
    this.cleanUser();
    this.showUser=true;
  }

  cleanUser() {
      this.userFormUser.firstName = '';
      this.userFormUser.lastName = '';
      this.userFormUser.npiNumber ='';
      this.userFormUser.phone='';
      this.userFormUser.email='';
      this.userFormUser.businessAddress='';
  }

  cancelUserForm(user:User) {
    this.showUser=false;
  }
}
