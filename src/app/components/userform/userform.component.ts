import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})

export class UserformComponent implements OnInit {
  @Input() userFormUser:User;

  @Output() addNewUser:EventEmitter<User> = new EventEmitter();
  @Output() cancelAddUser:EventEmitter<User> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
