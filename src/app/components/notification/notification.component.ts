import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  constructor() { }

  @Input() messageToDisplay:string;

  @Output() DisplayMessage:EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }

}