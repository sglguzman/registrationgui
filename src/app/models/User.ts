export interface User{
  firstName:string,
  lastName:string,
  npiNumber:string,
  businessAddress: string,
  phone:string,
  email:string
}
